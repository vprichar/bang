HTTP-friendly error objects

<!-- toc -->

- [Bang](#bang)
  - [Helper Methods](#helper-methods)
    - [`wrap(error, [statusCode], [message])`](#wraperror-statuscode-message)
    - [`create(statusCode, [message], [data])`](#createstatuscode-message-data)
  - [HTTP 4xx Errors](#http-4xx-errors)
    - [`Bang.badRequest([message], [data])`](#bangbadrequestmessage-data)
    - [`Bang.unauthorized([message], [data])`](#bangunauthorizedmessage-data)
    - [`Bang.paymentRequired([message], [data])`](#bangpaymentrequiredmessage-data)
    - [`Bang.forbidden([message], [data])`](#bangforbiddenmessage-data)
    - [`Bang.notFound([message], [data])`](#bangnotfoundmessage-data)
    - [`Bang.methodNotAllowed([message], [data], [allow])`](#bangmethodnotallowedmessage-data-allow)
    - [`Bang.notAcceptable([message], [data])`](#bangnotacceptablemessage-data)
    - [`Bang.proxyAuthRequired([message], [data])`](#bangproxyauthrequiredmessage-data)
    - [`Bang.clientTimeout([message], [data])`](#bangclienttimeoutmessage-data)
    - [`Bang.conflict([message], [data])`](#bangconflictmessage-data)
    - [`Bang.resourceGone([message], [data])`](#bangresourcegonemessage-data)
    - [`Bang.lengthRequired([message], [data])`](#banglengthrequiredmessage-data)
    - [`Bang.preconditionFailed([message], [data])`](#bangpreconditionfailedmessage-data)
    - [`Bang.entityTooLarge([message], [data])`](#bangentitytoolargemessage-data)
    - [`Bang.uriTooLong([message], [data])`](#banguritoolongmessage-data)
    - [`Bang.unsupportedMediaType([message], [data])`](#bangunsupportedmediatypemessage-data)
    - [`Bang.rangeNotSatisfiable([message], [data])`](#bangrangenotsatisfiablemessage-data)
    - [`Bang.expectationFailed([message], [data])`](#bangexpectationfailedmessage-data)
    - [`Bang.teapot([message], [data])`](#bangteapotmessage-data)
    - [`Bang.badData([message], [data])`](#bangbaddatamessage-data)
    - [`Bang.locked([message], [data])`](#banglockedmessage-data)
    - [`Bang.preconditionRequired([message], [data])`](#bangpreconditionrequiredmessage-data)
    - [`Bang.tooManyRequests([message], [data])`](#bangtoomanyrequestsmessage-data)
    - [`Bang.illegal([message], [data])`](#bangillegalmessage-data)
  - [HTTP 5xx Errors](#http-5xx-errors)
    - [`Bang.badImplementation([message], [data])` - (*alias: `internal`*)](#bangbadimplementationmessage-data---alias-internal)
    - [`Bang.notImplemented([message], [data])`](#bangnotimplementedmessage-data)
    - [`Bang.badGateway([message], [data])`](#bangbadgatewaymessage-data)
    - [`Bang.serverUnavailable([message], [data])`](#bangserverunavailablemessage-data)
    - [`Bang.gatewayTimeout([message], [data])`](#banggatewaytimeoutmessage-data)

<!-- tocstop -->

# Bang

**bang** provides a set of utilities for returning HTTP errors. Each utility returns a `Bang` error response
object (instance of `Error`) which includes the following properties:
- `statusCode` - the HTTP status code, derived from `error.output.statusCode`.
- `error` - the HTTP status message (e.g. 'Bad Request', 'Internal Server Error') derived from `statusCode`.
- `message` - the error message derived from `error.message`.

## Helper Methods

### `wrap(error, [statusCode], [message])`

Decorates an error with the **bang** properties where:
- `error` - the error object to wrap. If `error` is already a **bang** object, returns back the same object.
- `statusCode` - optional HTTP status code. Defaults to `500`.
- `message` - optional message string. If the error already has a message, it adds the message as a prefix.
  Defaults to no message.

```js
var error = new Error('Unexpected input');
Bang.wrap(error, 400);
```

### `create(statusCode, [message], [data])`

Generates an `Error` object with the **bang** decorations where:
- `statusCode` - an HTTP error code number. Must be greater or equal 400.
- `message` - optional message string.
- `data` - additional error data set to `error.data` property.

```js
var error = Bang.create(400, 'Bad request', { timestamp: Date.now() });
```

## HTTP 4xx Errors

### `Bang.badRequest([message], [data])`

Returns a 400 Bad Request error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.badRequest('invalid query');
```

Generates the following response payload:

```json
{
    "statusCode": 400,
    "error": "Bad Request",
    "message": "invalid query"
}
```

### `Bang.unauthorized([message], [data])`

Returns a 401 Unauthorized error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.unauthorized('invalid password');
```

Generates the following response:

```json
"payload": {
    "statusCode": 401,
    "error": "Unauthorized",
    "message": "invalid password"
}
```

### `Bang.paymentRequired([message], [data])`

Returns a 402 Payment Required error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.paymentRequired('bandwidth used');
```

Generates the following response payload:

```json
{
    "statusCode": 402,
    "error": "Payment Required",
    "message": "bandwidth used"
}
```

### `Bang.forbidden([message], [data])`

Returns a 403 Forbidden error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.forbidden('try again some time');
```

Generates the following response payload:

```json
{
    "statusCode": 403,
    "error": "Forbidden",
    "message": "try again some time"
}
```

### `Bang.notFound([message], [data])`

Returns a 404 Not Found error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.notFound('missing');
```

Generates the following response payload:

```json
{
    "statusCode": 404,
    "error": "Not Found",
    "message": "missing"
}
```

### `Bang.methodNotAllowed([message], [data], [allow])`

Returns a 405 Method Not Allowed error where:
- `message` - optional message.
- `data` - optional additional error data.
- `allow` - optional string or array of strings (to be combined and separated by ', ') which is set to the 'Allow' header.

```js
Bang.methodNotAllowed('that method is not allowed');
```

Generates the following response payload:

```json
{
    "statusCode": 405,
    "error": "Method Not Allowed",
    "message": "that method is not allowed"
}
```

### `Bang.notAcceptable([message], [data])`

Returns a 406 Not Acceptable error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.notAcceptable('unacceptable');
```

Generates the following response payload:

```json
{
    "statusCode": 406,
    "error": "Not Acceptable",
    "message": "unacceptable"
}
```

### `Bang.proxyAuthRequired([message], [data])`

Returns a 407 Proxy Authentication Required error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.proxyAuthRequired('auth missing');
```

Generates the following response payload:

```json
{
    "statusCode": 407,
    "error": "Proxy Authentication Required",
    "message": "auth missing"
}
```

### `Bang.clientTimeout([message], [data])`

Returns a 408 Request Time-out error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.clientTimeout('timed out');
```

Generates the following response payload:

```json
{
    "statusCode": 408,
    "error": "Request Time-out",
    "message": "timed out"
}
```

### `Bang.conflict([message], [data])`

Returns a 409 Conflict error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.conflict('there was a conflict');
```

Generates the following response payload:

```json
{
    "statusCode": 409,
    "error": "Conflict",
    "message": "there was a conflict"
}
```

### `Bang.resourceGone([message], [data])`

Returns a 410 Gone error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.resourceGone('it is gone');
```

Generates the following response payload:

```json
{
    "statusCode": 410,
    "error": "Gone",
    "message": "it is gone"
}
```

### `Bang.lengthRequired([message], [data])`

Returns a 411 Length Required error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.lengthRequired('length needed');
```

Generates the following response payload:

```json
{
    "statusCode": 411,
    "error": "Length Required",
    "message": "length needed"
}
```

### `Bang.preconditionFailed([message], [data])`

Returns a 412 Precondition Failed error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.preconditionFailed();
```

Generates the following response payload:

```json
{
    "statusCode": 412,
    "error": "Precondition Failed"
}
```

### `Bang.entityTooLarge([message], [data])`

Returns a 413 Request Entity Too Large error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.entityTooLarge('too big');
```

Generates the following response payload:

```json
{
    "statusCode": 413,
    "error": "Request Entity Too Large",
    "message": "too big"
}
```

### `Bang.uriTooLong([message], [data])`

Returns a 414 Request-URI Too Large error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.uriTooLong('uri is too long');
```

Generates the following response payload:

```json
{
    "statusCode": 414,
    "error": "Request-URI Too Large",
    "message": "uri is too long"
}
```

### `Bang.unsupportedMediaType([message], [data])`

Returns a 415 Unsupported Media Type error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.unsupportedMediaType('that media is not supported');
```

Generates the following response payload:

```json
{
    "statusCode": 415,
    "error": "Unsupported Media Type",
    "message": "that media is not supported"
}
```

### `Bang.rangeNotSatisfiable([message], [data])`

Returns a 416 Requested Range Not Satisfiable error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.rangeNotSatisfiable();
```

Generates the following response payload:

```json
{
    "statusCode": 416,
    "error": "Requested Range Not Satisfiable"
}
```

### `Bang.expectationFailed([message], [data])`

Returns a 417 Expectation Failed error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.expectationFailed('expected this to work');
```

Generates the following response payload:

```json
{
    "statusCode": 417,
    "error": "Expectation Failed",
    "message": "expected this to work"
}
```

### `Bang.teapot([message], [data])`

Returns a 418 I'm a Teapot error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.teapot('sorry, no coffee...');
```

Generates the following response payload:

```json
{
    "statusCode": 418,
    "error": "I'm a Teapot",
    "message": "Sorry, no coffee..."
}
```

### `Bang.badData([message], [data])`

Returns a 422 Unprocessable Entity error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.badData('your data is bad and you should feel bad');
```

Generates the following response payload:

```json
{
    "statusCode": 422,
    "error": "Unprocessable Entity",
    "message": "your data is bad and you should feel bad"
}
```

### `Bang.locked([message], [data])`

Returns a 423 Locked error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.locked('this resource has been locked');
```

Generates the following response payload:

```json
{
    "statusCode": 423,
    "error": "Locked",
    "message": "this resource has been locked"
}
```

### `Bang.preconditionRequired([message], [data])`

Returns a 428 Precondition Required error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.preconditionRequired('you must supply an If-Match header');
```

Generates the following response payload:

```json
{
    "statusCode": 428,
    "error": "Precondition Required",
    "message": "you must supply an If-Match header"
}
```

### `Bang.tooManyRequests([message], [data])`

Returns a 429 Too Many Requests error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.tooManyRequests('you have exceeded your request limit');
```

Generates the following response payload:

```json
{
    "statusCode": 429,
    "error": "Too Many Requests",
    "message": "you have exceeded your request limit"
}
```

### `Bang.illegal([message], [data])`

Returns a 451 Unavailable For Legal Reasons error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.illegal('you are not permitted to view this resource for legal reasons');
```

Generates the following response payload:

```json
{
    "statusCode": 451,
    "error": "Unavailable For Legal Reasons",
    "message": "you are not permitted to view this resource for legal reasons"
}
```

## HTTP 5xx Errors

All 500 errors hide your message from the end user. Your message is recorded in the server log.

### `Bang.badImplementation([message], [data])` - (*alias: `internal`*)

Returns a 500 Internal Server Error error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.badImplementation('terrible implementation');
```

Generates the following response payload:

```json
{
    "statusCode": 500,
    "error": "Internal Server Error",
    "message": "An internal server error occurred"
}
```

### `Bang.notImplemented([message], [data])`

Returns a 501 Not Implemented error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.notImplemented('method not implemented');
```

Generates the following response payload:

```json
{
    "statusCode": 501,
    "error": "Not Implemented",
    "message": "method not implemented"
}
```

### `Bang.badGateway([message], [data])`

Returns a 502 Bad Gateway error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.badGateway('that is a bad gateway');
```

Generates the following response payload:

```json
{
    "statusCode": 502,
    "error": "Bad Gateway",
    "message": "that is a bad gateway"
}
```

### `Bang.serverUnavailable([message], [data])`

Returns a 503 Service Unavailable error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.serverUnavailable('unavailable');
```

Generates the following response payload:

```json
{
    "statusCode": 503,
    "error": "Service Unavailable",
    "message": "unavailable"
}
```

### `Bang.gatewayTimeout([message], [data])`

Returns a 504 Gateway Time-out error where:
- `message` - optional message.
- `data` - optional additional error data.

```js
Bang.gatewayTimeout();
```

Generates the following response payload:

```json
{
    "statusCode": 504,
    "error": "Gateway Time-out"
}
```

---
